#Wir oeffnen die Datei kapital.txt für Lesen (read) - deswegen r
f = open("kapital_kurz.txt", "r")

text = f.read()
#Wir koennen ueberpruefen, ob das Lesen funktioniert:
#print(text)

string = "des"

#Befindet sich dat string im Text?
if string in text:
    print("Ja")
else:
    print("Nein")

#Wievielmal ist es in der Datei?
counter = text.count(string)
print(counter)

#####Welches Wort ist am häufigsten?
worterliste  = []
einzigartige = []

#Warnung: wird lang dauern
worterliste = text.split()
for i in worterliste:
    if i not in einzigartige:
        einzigartige.append(i)

print("Die Anzahl der Wörter in Das Kapital: "                  + str(len(worterliste)))
print("Die Anzahl einzigartiger Wörter in Das Kapital: "        + str(len(einzigartige)))

counter = 0
haufigsteswort   = 0
for wort in einzigartige:
    if text.count(wort) > counter:
        counter = text.count(wort)
        haufigsteswort = wort

print("Das häufigste Wort in Das Kapital ist: " + haufigsteswort)
print("Es wird " + str(counter) + "mal wiedertholt")
