# Python course

Here you can the
[script](https://gitlab.com/asz-it-course/asz_it_kurs/-/jobs/artifacts/main/raw/Python/script.pdf?job=compile_pdf_Python)
for our Python course. Currently it is only in German, because we thought that
there is already too much English material anyway.

Soon we wil add a method of translating it.
