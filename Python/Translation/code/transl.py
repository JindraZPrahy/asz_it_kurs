import deepl
import os

#Hier benutzt man environment variables fuer Sicherheit
translator = deepl.Translator(os.getenv("DEEPL_AUTH_KEY"))
#ODER
#translator = deepl.Translator(YOURKEY)

result = translator.translate_text("Hello, world!", target_lang="FR")
print(result)  # "Bonjour, le monde !"


