# ASZ_IT_Kurs

(English below)
Hier gibt es Materialen für den ASZ IT Kurs.

Wir haben eine webseite: [https://asz-it-course.gitlab.io/asz_web/en/index.html](https://asz-it-course.gitlab.io/asz_web/en/index.html).


## English

Here you can find materials for the [ASZ IT](https://www.bildung-fuer-alle.ch/) course.
Visit us in person according to our [schedule](https://www.bildung-fuer-alle.ch/seite/stundenplan) (Computer
Einführungskurs), or just drop by the ASZ at Sihlquai 125, Zürich.

This is mostly a repository for the moderators, but it can also be used by students
to access materials they need.

What you can find here:
* A [file with general resources](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/blob/main/resources/resources.pdf) related to programming
* A [git tutorial](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/blob/main/git/git_basics.pdf)
* [Exercises for LibreOffice](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/tree/main/office)
* [Template for a CV with LibreOffice](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/tree/main/office/Lebenslauf_Muster.odt)
* An [introductory course on Python](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/tree/main/Python) (under construction)


## TODOs

1. Create a document explaining what an ad-blocking extension is + how to instal it.
2. Create a basic guide for bash similar to the [git guide](https://gitlab.com/JindraZPrahy/asz_it_kurs/-/tree/main/git).
3. Create a simple guide that students could use to translate documents.
4. Create a repository with resources for writing administrative e-mails. Potentially incorporate
an AI writer?
